# Apex Trading Coding Challenge

this is a coding challenge for apex trading

> ## Dependencies
>
> - docker
> - docker-compose

> ## Running
>
> ```bash
> docker-compose up
> ```
>
> code challenge can be viewed from http://locahost:8080

> ## Troubleshooting
>
> if for some reason you get a permission issue regarding the node_modules (osx & linux only) modify the .env file with your UID and GID. you can use the `id` command to find your own UID and GID
