import React, { useState, useEffect } from 'react';
import { Container, Row, Table, Button } from 'react-bootstrap';
import axios from 'axios';
import '../styles/App.css';

import PermissionModal from './PermissionModal';

function App() {
  const [users, setUsers] = useState([]);

  const updateUser = (updatedUser) =>
    axios
      .post('/api/user/update', updatedUser)
      .then((result) => setUsers(result.data));

  useEffect(() => {
    axios.get('/api/users').then((users) => setUsers(users.data));
  }, [setUsers]);

  return (
    <Container id="app-container" className="raised">
      <Row>
        <h5 id="app-title">Bundy Apex Farms Users ({users.length})</h5>
      </Row>
      <Row>
        <Table style={{ marginBottom: '0' }}>
          <thead style={{ background: 'lightgray' }}>
            <tr>
              <th></th>
              <th>Name</th>
              <th className="text-align-center">Role</th>
              <th className="text-align-center">Permissions</th>
            </tr>
          </thead>
          <tbody>
            {users &&
              users.map((user) => (
                <tr key={user.name}>
                  <td className="user-icon">
                    <div>
                      <i className="fas fa-user"></i>
                    </div>
                  </td>
                  <td>{user.name}</td>
                  <td className="text-align-center">{user.role}</td>
                  <td className="text-align-center">
                    <PermissionModal updateUser={updateUser} user={user} />
                  </td>
                </tr>
              ))}
          </tbody>
        </Table>
      </Row>
    </Container>
  );
}

export default App;
